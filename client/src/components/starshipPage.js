import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import '../App.css';

import { createBrowserHistory } from "history";
const history = createBrowserHistory();

const StarshipPage = ({ match }) => {

  const {
    params: { starshipId },
  } = match;
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState();

  useEffect(() => {
    // console.log(starshipId)
    fetch(`https://swapi.dev/api/starships/${starshipId}`, {})
      .then((res) => res.json())
      .then((response) => {
        setData(response);
        setIsLoading(false);
        // console.log(`https://swapi.dev/api/starships/${starshipId}`)
      })
      .catch((error) => console.log(error));
  }, [starshipId]);

  return (
    <div>
      {!isLoading && (
        <div className="charPage" >
        <div>
          {data.detail === "Not found" ?
            <h2>Detail not found</h2>
          : <div>
              <h1 className="display-3">{data.name}</h1>
              <h3>manufacturer: {data.manufacturer}</h3>
              <div>hyperdrive_rating: {data.hyperdrive_rating}</div>
              <div>model: {data.model}</div>
              <div>passengers: {data.passengers}</div>
              <div>starship_class: {data.starship_class}</div>
              <div>crew: {data.crew}</div>
              <div>MGLT: {data.MGLT}</div>
            </div>
          }
        </div>
        <button onClick={history.goBack} className="btn btn-secondary mr-2 mt-5">
              Back to character's page
        </button>
          <Link to="/">
            <button className="btn btn-primary mt-5">
              Back to homepage
            </button>
          </Link>
        </div>
      )}
    </div>
  );
};

export default StarshipPage
