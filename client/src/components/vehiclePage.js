import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import '../App.css';

import { createBrowserHistory } from "history";
const history = createBrowserHistory();

const VehiclePage = ({ match }) => {

console.log(match)
  const {
    params: { vehicleId },
  } = match;
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState();

  useEffect(() => {

    fetch(`https://swapi.dev/api/vehicles/${vehicleId}`, {})
      .then((res) => res.json())
      .then((response) => {
        setData(response);
        setIsLoading(false);
        // console.log(data)
      })
      .catch((error) => console.log(error));
  }, []);

  return (
    <div>
      {!isLoading && (
        <div className="charPage" >
        <div>
          {data.detail === "Not found" ?
            <h2>Detail not found</h2>
          : <div>
              <h1 className="display-3">{data.model}</h1>
              <h1 className="droid-font mb-5">{data.model}</h1>
              <h5>manufacturer: {data.manufacturer}</h5>
            </div>
          }
        </div>
        <button onClick={history.goBack} className="btn btn-secondary mr-2 mt-5">
              Back to character's page
        </button>
          <Link to="/">
            <button className="btn btn-primary mt-5">
              Back to homepage
            </button>
          </Link>
        </div>
      )}
    </div>
  );
};

export default VehiclePage
