import { Link } from "react-router-dom";

const LinksToPages = (props) => {


  return (
    <div>
    {(props.data.length) ?
              <ul>
                {props.address.toUpperCase()}: {
                  props.data.map((el, index) => {
                    return(
                    <Link to={`/${props.address}/${index + 1}`} key={index}>
                      <button className="btn btn-secondary mr-1 ml-2">{index + 1}</button>
                    </Link>
                  )})
                }
              </ul>
              : <p></p>
              }

    </div>
    )
}

export default LinksToPages
