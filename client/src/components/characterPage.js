import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import '../App.css';
import LinksToPages from './LinksToPages'

const CharacterPage = ({ match }) => {

  const {
    params: { personId },
  } = match;
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState();

  useEffect(() => {
    fetch(`https://swapi.dev/api/people/${personId}`, {})
      .then((res) => res.json())
      .then((response) => {
        setData(response);
        setIsLoading(false);
        // console.log(`https://swapi.dev/api/people/${personId}`)
      })
      .catch((error) => console.log(error));
  }, [personId]);

  return (
    <div>
      {isLoading && (
        <div>...Loading</div>
        )}
      {!isLoading && (
        <div className="charPage" >
          <h1 className="display-3">{data.name}</h1>
          <h1 className="droid-font mb-5">{data.name}</h1>
          <h4>Height: {data.height}</h4>
          <h4>Mass: {data.mass}</h4>
          <h4>Hair Color: {data.hair_color}</h4>
          <h4>Skin Color: {data.skin_color}</h4>
          <h4>Eye Color: {data.eye_color}</h4>
          <h4>Birth Year: {data.birth_year}</h4>
          <h4>Gender: {data.gender}</h4>

          <LinksToPages data={data.starships} address={'starships'}/>
{/*          <LinksToPages data={data.vehicles} address={'vehicles'}/>*/}
          <LinksToPages data={data.films} address={'films'}/>

          <Link to="/">
            <button className="btn btn-secondary mt-5">
              Back to homepage
            </button>
          </Link>
        </div>
      )}
    </div>
  );
};

export default CharacterPage
