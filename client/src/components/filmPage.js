import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import '../App.css';


import { createBrowserHistory } from "history";
const history = createBrowserHistory();

const FilmPage = ({ match }) => {

  const {
    params: { filmId },
  } = match;
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState();

  useEffect(() => {
    fetch(`https://swapi.dev/api/films/${filmId}`, {})
      .then((res) => res.json())
      .then((response) => {
        setData(response);
        setIsLoading(false);
        // console.log(`https://swapi.dev/api/films/${filmId}`)
      })
      .catch((error) => console.log(error));
  }, [filmId]);

  return (
    <div>
      {!isLoading && (
        <div className="container">
        <div className="row title">
          <h1 className="display-3 col-8">{data.title}</h1>
        </div>
        <div className="row">
          <h4 className="col-4">{data.director}</h4>
        </div>
          <div className="row film_crawl">
            <p>{data.opening_crawl}</p>
          </div>
            <button onClick={history.goBack} className="btn btn-secondary mr-2 mt-5">
              Back to character's page
            </button>
          <Link to="/">
            <button className="btn btn-primary mt-5">
              Back to homepage
            </button>
          </Link>
        </div>
      )}
    </div>
  );
};

export default FilmPage
