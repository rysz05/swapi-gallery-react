import React from 'react';
import '../App.css';

const Card = (props) => {

	//console.log(props.data)

	return (
	<div className="card mb-3">
						<h3 className="card-title">{props.data.name}</h3>
					<div className="card-body">
						<p>Birth year: {props.data.birth_year}</p>
						<p>Hair color: {props.data.hair_color}</p>
						<p>Height: {props.data.height}</p>
						<p>Mass: {props.data.mass}</p>
						<p>Skin color: {props.data.skin_color}</p>
					</div>
				</div>
	)
}

export default Card;
