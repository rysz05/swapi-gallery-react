import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Start from './pages/Start';
import CharacterPage from './components/characterPage';
import VehiclePage from './components/vehiclePage'
import StarshipPage from './components/starshipPage'
import FilmPage from './components/filmPage'


const App = () => {


  return (
    <div>
        <Switch>
          <Route exact path='/' component={Start}/>
          <Route path='/person/:personId' component={CharacterPage} />
          <Route path='/vehicles/:vehicleId' component={VehiclePage} />
          <Route path='/films/:filmId' component={FilmPage} />
          <Route path='/starships/:starshipId' component={StarshipPage} />
        </Switch>
    </div>
    )
}


export default App;
