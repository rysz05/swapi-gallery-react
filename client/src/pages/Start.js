import React, { useState, useEffect } from 'react';
import logo from "../Star_Wars-Logo.wine.svg"
import Card from "../components/Card"
import { Link } from "react-router-dom";

function Start() {

	const [characters, setCharacters] = useState([]);
	const [showCharacters, setShowCharacters] = useState([])
	const [isLoading, setIsLoading] = useState(true);
	const [input, setInput] = useState('')

	useEffect(() => {

	    fetch("https://swapi.dev/api/people/", {})
	      .then((res) => res.json())
	      .then((response) => {
	      	console.log(response.results)
	      	response.results.forEach((el, i) => {
	      		el.id = i + 1
	      	})
	      	// console.log(response.results)
	        setCharacters(response.results);
	        setShowCharacters(response.results)
	        setIsLoading(false);
	      })
	      .catch((error) => console.log(error));
	  }, []);

	const handleInputChange = event => {
	    const input = event.target.value
	    setInput(input)
	    // console.log(input)
	    const res = characters.filter((el) => el.name.toLowerCase().includes(input.toLowerCase()));
	    setShowCharacters(res);
  };


	return (

			<div className="container mb-5">
		      <div className="title mb-5">
		        <img className="logo" src={logo} alt="star wars logo"/>
		        <h1 id="logo-title" className="display-5 droid-font">SUSPECTS</h1>
		      </div>
		      <div className="row input-group">
		        <h4 className="droid-font label col-4">Filter results</h4>
		        <input
		        className="form-control col-8"
		        type="text"
		        name="filter"
		        value={input}
		        onChange={handleInputChange}
		        autoFocus
		        autoComplete= "off"
		        />
		      </div>
		      <div id="gallery" className="row mt-3">{!isLoading &&
		        showCharacters.map((person, index) => {
		        	return (
		        	<Link to={`/person/${person.id}`} key={index}>
			          <Card data={person}/>
			        </Link>
		        	)
		        })}
		      </div>
		  </div>

		)
}

export default Start
