const express = require("express");
const path = require('path');

const PORT = process.env.PORT || 3004;

const app = express();

app.get("/", (req, res) => {
  res.json({ message: "Hello from server!" });
});

app.use(express.static(path.resolve(__dirname, '../client/build')));


app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
